## React Boilerplate

I used this boilerplate as starting point: https://github.com/CrocoDillon/universal-react-redux-boilerplate.git

One of the most efficient React Boilerplate, it integrates a lot of features useful for scalable application: Server-side rendering, Redux, Hot Reloader, React Route, ....

Even if the requirements points of the test could have been achieved with the default React boilerplate, With this one it was easier for me to achieve the bonus points like server-side rendering, use of SASS and SEO optimisations.

The only thing I've added is the airbnb Eslint preset. Even if I had some issues with this linter, probably a lake of configuration but I didn't wan't to waste too much time on this point.

## Running the development server
Install the dependencies first

```
npm install
```

And then

```bash
npm run dev
```
Then launch in your browser `http://localhost:3000`

## Access the content

One of the requirement was to use the content from the json object. To achieve that and simulate an API endpoint as it's usually the case, I've used a tiny Koa API. The content is accessible here `http://localhost:3000/api/widget` once the dev server launched.

## Optimisations

*  I displayed the title of the current item within the content. It wasn't in the screenshot provided but I think it's important to keep the title of the active question.
*  I decided to host the image on an external server to simulate the use of a CDN as it should be on an application.

## Running the production server

```bash
npm run build
npm start
```

## Thank You!

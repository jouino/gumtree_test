/* eslint-env mocha */
/* global expect, sinon */
import React from 'react'
import { shallow } from 'enzyme'

import { Widget } from './Widget'

describe('<Widget />', () => {

  function setup(props) {
    props = {
      widgetContent: [],
      ...props
    }

    const wrapper = shallow(<Widget { ...props } />)
    const instance = wrapper.instance()

    return { wrapper, instance }
  }

  it('renders', () => {
    const { wrapper, instance } = setup()

    expect(wrapper).to.be.ok
    expect(instance).to.be.ok
  })

  it('renders list of widgetContent', () => {
    const widgetContent = [
      {
        title: 'title1'
      },
      {
        title: 'title2'
      },
      {
        title: 'title3'
      }
    ]
    const { wrapper } = setup({ widgetContent })

    expect(wrapper.find('li')).to.have.length(widgetContent.length)
  })
})

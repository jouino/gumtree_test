// @flow
import React from 'react';

export const WidgetHeader = (props: Object) => {
  const { toggleAction, title } = props
  const styles = require('../widget.scss')

  return (
    <h1 className={ styles.widget__header }>
      <button onClick={toggleAction} className={ styles.widget__headerButton } >
        {title}
      </button>
    </h1>
  )
}

WidgetHeader.displayName = 'WidgetHeader';

export default WidgetHeader;

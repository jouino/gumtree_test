// @flow
import React from 'react';

// Display the right content if client rendering, display all the widget content if server side rendering
export const WidgetHeader = (props: Object) => {
  const { activeIndex, isServerSide, content } = props
  const styles = require('../widget.scss')

  return (
    <article className={ styles.widget__content }>
      { content.filter((c, i) => (i === activeIndex && !isServerSide) || isServerSide).map(({ title, thumbnail, description }, i) => (
        <div key={ i } className={ styles.widget__contentContainer }>

          { thumbnail &&
            <div className={ styles.widget__contentContainerImage }>
              <img src={ thumbnail } className={ styles.widget__contentImage } />
            </div>
          }

          <div className={ styles.widget__contentMetaContainer }>
            <h2 className={ styles.widget__contentMetaTitle }>{ title }</h2>
            <p className={ styles.widget__contentMetaDesc } dangerouslySetInnerHTML={{__html: description}} />
          </div>
        </div>
      )) }
    </article>
  )
}

WidgetHeader.displayName = 'WidgetHeader';

export default WidgetHeader;

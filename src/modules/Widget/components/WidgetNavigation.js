// @flow
import React from 'react';

// hide the navigation if server
export const WidgetHeader = (props: Object) => {
  const { content, activeIndex, prevAction, nextAction } = props
  const styles = require('../widget.scss')

  return (
    <div className={ styles.widget__navigation }>
      <div>
        { content.filter((c, i) => (activeIndex === 0 && i === content.length - 1) || (activeIndex < content.length && i === activeIndex - 1)).map((c, i) => (
          <button key={ i } onClick={ prevAction } className={ [styles.widget__navigationButton, styles['widget__navigationButton--prev']].join(' ') } >
            { c.title }
          </button>
        )) }
      </div>
      <div>
        { content.filter((c, i) => (activeIndex + 1 === content.length && i === 0) || (activeIndex < content.length && i === activeIndex + 1)).map((c, i) => (
          <button key={ i } onClick={ nextAction } className={ [styles.widget__navigationButton, styles['widget__navigationButton--next']].join(' ') } >
            { c.title }
          </button>
        )) }
      </div>
    </div>
  )
}

WidgetHeader.displayName = 'WidgetHeader';

export default WidgetHeader;

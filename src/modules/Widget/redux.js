import { combineReducers } from 'redux'
import fetch from 'isomorphic-fetch'

const FETCH_CONTENT_REQUEST = 'widget/FETCH_CONTENT_REQUEST'
const FETCH_CONTENT_SUCCESS = 'widget/FETCH_CONTENT_SUCCESS'
const FETCH_CONTENT_FAILURE = 'widget/FETCH_CONTENT_FAILURE'

const widgetContent = (state = {
  isLoading: false,
  title: '',
  content: [],
}, action) => {
  switch (action.type) {
  case FETCH_CONTENT_SUCCESS:
    return Object.assign({}, state, {
      title: action.response.title,
      content: action.response.content,
      isLoading: false
    })
  case FETCH_CONTENT_REQUEST:
    return Object.assign({}, state, {
      isLoading: true
    })
  case FETCH_CONTENT_FAILURE:
    return null
  }

  return state
}

const reducer = combineReducers({
  widgetContent
})

const fetchAction = (url, types) => dispatch => {
  const [REQUEST, SUCCESS, FAILURE] = types

  if (__SERVER__) {
    url = `http://localhost:3000${ url }`
  }

  dispatch({
    type: REQUEST
  })

  return fetch(url)
    .then(
      response => response.json()
    )
    .then(
      response => dispatch({
        type: SUCCESS,
        response
      }),
      error => dispatch({
        type: FAILURE,
        message: error.message
      })
    )
}

export const fetchContent = () => fetchAction(
  '/api/widget',
  [FETCH_CONTENT_REQUEST, FETCH_CONTENT_SUCCESS, FETCH_CONTENT_FAILURE]
)

export const getWidgetContent = state => state.widget.widgetContent.content
export const getWidgetTitle = state => state.widget.widgetContent.title
export const getLoading = state => state.blog.widgetContent === 'loading'

export default reducer

// @flow
import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'

// Components
import WidgetHeader from './components/WidgetHeader.js'
import WidgetContent from './components/WidgetContent.js'
import WidgetNavigation from './components/WidgetNavigation.js'

// Redux
import { fetchContent, getWidgetContent, getWidgetTitle } from './redux'

// Helpers
import { serverSideRendering } from '../../helpers/serverSide.js'

const styles = require('./widget.scss')

class Widget extends React.Component {
  static displayName = 'Widget'

  static propTypes = {
    widgetContent: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        thumbnail: PropTypes.string,
        description: PropTypes.string.isRequired
      })
    ).isRequired,
    widgetTitle: PropTypes.string.isRequired,
    onEnter: PropTypes.func,
  }

  state = {
    activeIndex: 0,
    accordionOpen: true
  }

  static onEnter = ({ dispatch }) => dispatch(fetchContent())

  // Get the right index if click on Next
  updateNext = () => {
    const { activeIndex } = this.state
    const { widgetContent } = this.props
    // Check if current index is last one of the list
    if ((activeIndex + 1) === widgetContent.length) {
      this.setState({ activeIndex: 0 })
    } else {
      this.setState({ activeIndex: activeIndex + 1 })
    }
  }

  // Get the right index if click on Prev
  updatePrev = () => {
    const { activeIndex } = this.state
    const { widgetContent } = this.props
    // Check if current index is the first one of the list
    if ((activeIndex - 1) < 0) {
      this.setState({ activeIndex: (widgetContent.length - 1) })
    } else {
      this.setState({ activeIndex: activeIndex - 1 })
    }
  }

  // Toggle the accordion
  toggleAccordion = () => {
    this.setState({
      accordionOpen: !this.state.accordionOpen
    })
  }

  render() {
    const { activeIndex, accordionOpen } = this.state
    const { widgetTitle, widgetContent } = this.props

    return (
      <section className={ [styles.widget, (!accordionOpen) ? styles['widget--inactive'] : ''].join(' ') }>
        <Helmet title="Widget Box" />
        <WidgetHeader
          title={ widgetTitle }
          toggleAction={ this.toggleAccordion }
        />
        <div className={ styles.widget__container } >
          <WidgetContent
            content={ widgetContent }
            activeIndex={ activeIndex }
            isServerSide={ serverSideRendering() }
          />
          { !serverSideRendering() &&
            <WidgetNavigation
              content={ widgetContent }
              activeIndex={ activeIndex }
              prevAction= { this.updatePrev }
              nextAction= { this.updateNext }
            />
          }
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => ({
  widgetContent: getWidgetContent(state),
  widgetTitle: getWidgetTitle(state)
})

export default connect(mapStateToProps)(Widget)

// @flow
import React from 'react'
import Helmet from 'react-helmet'

import { Link } from 'react-router'

const Home = () => (
  <main>
    <Helmet title="Home" />
    <h1>Hello Gumtree!</h1>
    <p>All the explanations are in the README file.</p>
    <p>
      <Link to={ `/widget/` }>Access to the widget</Link>
    </p>
  </main>
)

Home.displayName = 'Home'

export default Home

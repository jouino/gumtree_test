import { combineReducers } from 'redux';

import { reducer as widget } from './modules/Widget';

const reducer = combineReducers({
  widget,
});

export default reducer;

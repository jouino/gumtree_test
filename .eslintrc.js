module.exports = {
  "extends": "airbnb",
  "rules": {
    "indent": ["error", 2],
    "func-names": 0,
    "no-console": 0,
    "max-len": ["error", 500]
  }
}
